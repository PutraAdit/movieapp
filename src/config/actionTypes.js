export const LOAD_MOVIES = 'LOAD_MOVIES'
export const MOVIES_LOADED = 'MOVIES_LOADED'
export const ADD_MOVIE = 'ADD_MOVIE'
export const REMOVE_MOVIE = 'REMOVE_MOVIE'