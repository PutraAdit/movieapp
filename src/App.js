import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { HashRouter as Router, Route } from 'react-router-dom';

import './App.css';

import Navbar from './components/layout/Navbar';

import Landing from './components/home/Landing';
import MyMovieLanding from './components/home/MyMovieLanding';
import Movie from './components/home/Movie';

import store from './store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Navbar />
            <Route exact path="/" component={Landing} />
            <Route exact path="/movie/:id" component={Movie} />
            <Route exact path="/my-movie" component={MyMovieLanding} />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
