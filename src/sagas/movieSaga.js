import { takeEvery, call, put, all } from 'redux-saga/effects'

import * as types from '../config/actionTypes'
import * as service from '../services/MovieService'
import * as actions from '../actions/searchActions'

function* loadMovies({ data }) {
    try {
        const movies = yield call(service.getMovies, data)
        yield put(actions.moviesLoadedAction(movies))
    } catch (e) {
        console.log(e)
    }
}

function* watchLoadMovies() {
    yield takeEvery(types.LOAD_MOVIES, loadMovies)
}

export function* movieSaga() {
    yield all([
        watchLoadMovies()
    ])
}