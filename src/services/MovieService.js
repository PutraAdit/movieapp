import axios from 'axios'
import { baseURL, apiKey } from '../config/api'

export const getMovies = (text) => {
    return axios.get(baseURL, { params: { apikey: apiKey, s: text } }).then(res => res.data)
}

export const getSingleMovie = id => {
    return axios.get(baseURL, { params: { apikey: apiKey, i: id } }).then(res => res.data)
}