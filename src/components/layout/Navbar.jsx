import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <div> 
      <nav className="navbar navbar-light navbar-expand-lg bg-light mb-5">
        <div className="container">
          <div className="navbar-header">
            <Link className="navbar-brand text-lg brand-text" to="/">
              MovieApp
            </Link>
          </div>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto text-light d-inline-block">
              <li className="nav-item">
                <Link className="btn btn-primary text-white text-lg" to="/my-movie">
                  My Movie
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
