import React, { Component } from 'react';

import MyMovieContainer from './MyMovieContainer';

class MyMovieLanding extends Component {
  render() {
    return (
      <div className="container">
        <MyMovieContainer />
      </div>
    );
  }
}

export default MyMovieLanding;
