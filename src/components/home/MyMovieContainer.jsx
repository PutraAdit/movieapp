import React, { Component } from 'react';

import { connect } from 'react-redux';

import MyMovieCard from './MyMovieCard';

class MyMovieContainer extends Component {
  render() {
    const { mymovie } = this.props;
    let content = '';

    content =
      mymovie.length > 0
        ? mymovie.map((movie, index) => (
            <MyMovieCard key={index} movie={movie} />
          ))
        : null;
    return <div className="row">{content}</div>;
  }
}

const mapStateToProps = state => ({
  mymovie: state.mymovie
});

export default connect(mapStateToProps)(MyMovieContainer);
