import React, { Component } from 'react';

import { connect } from 'react-redux';

import MovieCard from './MovieCard';

class MoviesContainer extends Component {

  state = {
    currentPage: 1,
    perPage: 4
  };

  handleClick = (e) => {
    this.setState({
      currentPage: Number(e.target.id)
    });
  }

  render() {
    const { movies } = this.props;
    const { currentPage, perPage } = this.state
    const indexOfLastMovie = currentPage * perPage;
    const indexOfFirstMovie = indexOfLastMovie - perPage;
    if (movies.Search !== undefined) {
      const currentMovies = movies.Search.slice(indexOfFirstMovie, indexOfLastMovie);

      const renderMovie = movies.Response === 'True'
      ? currentMovies.map((movie, index) => (
          <MovieCard key={index} movie={movie} />
        ))
      : null;

      const pageNumbers = [];
      for (let i = 1; i <= Math.ceil(movies.Search.length / perPage); i++) {
        pageNumbers.push(i);
      }

      const renderPageNumbers = pageNumbers.map(number => {
        return (
          <li className="page-item" key={number}>
            <span className="page-link" id={number} onClick={this.handleClick}>{number}</span>
          </li>
        );
      });

      return (
        <>
          <div>
            <div className="row">
              {renderMovie}
            </div>
          </div>
          <nav>
            <ul className="pagination" id="page-numbers">
              {renderPageNumbers}
            </ul>
          </nav>
        </>
      )
    }

    return ''
  }
}

const mapStateToProps = state => ({
  movies: state.movies
});

export default connect(mapStateToProps)(MoviesContainer);
