import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

import { removeMovie } from '../../actions/searchActions'

class MovieCard extends Component {

  onDelete = (data) => {
    this.props.removeMovie(data)
  };

  render() {
    const { key, movie } = this.props;
    return (
      <div className="col-md-3 mb-5">
        <div className="card card-body bg-dark text-center h-100">
          <img className="w-100 mb-2" src={movie.Poster} alt="Movie Cover" />
          <h5 className="text-light card-title">
            {movie.Title} - {movie.Year}
          </h5>
          <Link className="btn btn-primary" to={'/movie/' + movie.imdbID}>
            Movie Details
            <i className="fas fa-chevron-right" />
          </Link>
          <button key={key} className="btn btn-primary btn-bg mt-3" onClick={() => this.onDelete(movie)}>
            Delete
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => { //store.dispatch
  return {
    removeMovie: (data) => {
      dispatch(removeMovie(data))
    }
  }
}

export default connect(null, mapDispatchToProps)(MovieCard)
