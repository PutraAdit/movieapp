import React, { Component } from 'react';

import { connect } from 'react-redux';

import { loadMoviesAction } from '../../actions/searchActions'

class SearchForm extends Component {
  state = {
    search: '',
  }

  onChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    })
  };

  onSubmit = e => {
    e.preventDefault()
    const { search } = this.state
    this.props.loadMovies(search)
    this.setState({
      search: '',
    })
    
  };

  render() {
    const { search } = this.state
    return (
      <div className="jumbotron jumbotron-fluid mt-5 text-center">
        <div className="container">
          <h1 className="display-4 mb-3">
            <i className="fa fa-search" /> Find Movies
          </h1>
          <form id="searchForm" onSubmit={this.onSubmit}>
            <input
              type="text"
              className="form-control"
              id="search"
              placeholder="Find Movies ..."
              value={search}
              onChange={this.onChange}
            />
            <button type="submit" className="btn btn-primary btn-bg mt-3">
              Search
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      loadMovies: (search) => {
        dispatch(loadMoviesAction(search))
      }
  }
}

export default connect(
  null,
  mapDispatchToProps
)(SearchForm);
