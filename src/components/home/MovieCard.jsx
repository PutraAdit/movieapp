import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

import { addMovie } from '../../actions/searchActions'

class MovieCard extends Component {

  onAdd = (data) => {
    this.props.addMovie(data)
  };

  render() {
    const { key, movie } = this.props;
    return (
      <div className="col-md-3 mb-5">
        <div className="card card-body bg-dark text-center h-100">
          <img className="w-100 mb-2" src={movie.Poster} alt="Movie Cover" />
          <h5 className="text-light card-title">
            {movie.Title} - {movie.Year}
          </h5>
          <Link className="btn btn-primary" to={'/movie/' + movie.imdbID}>
            Movie Details
            <i className="fas fa-chevron-right" />
          </Link>
          <button key={key} className="btn btn-primary btn-bg mt-3" onClick={() => this.onAdd(movie)}>
            Add
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => { //store.dispatch
  return {
    addMovie: (data) => {
      dispatch(addMovie(data))
    }
  }
}

export default connect(null, mapDispatchToProps)(MovieCard)
