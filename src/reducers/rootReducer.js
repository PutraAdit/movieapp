import { combineReducers } from 'redux'

import { movie, mymovie } from './movieReducer'

export default combineReducers({
    movies: movie,
    mymovie: mymovie
})