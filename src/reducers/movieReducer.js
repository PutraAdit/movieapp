import { MOVIES_LOADED, ADD_MOVIE, REMOVE_MOVIE } from '../config/actionTypes'

const movie = (state = [], action) => {
    if (action.type === MOVIES_LOADED) {
        return action.movies
    }

    return state
}

const mymovie = (state = [], action) => {
    console.log(action)
    if (action.type === ADD_MOVIE) {
        return state.concat(action.movie)
    }
    else if (action.type === REMOVE_MOVIE) {
        return state.filter( movie => movie.imdbID !== action.movie.imdbID)
    }

    return state
}

export { movie, mymovie }