import * as types from '../config/actionTypes'


export const loadMoviesAction = (data) => {
  return {
      type: types.LOAD_MOVIES,
      data
  }
}

export const moviesLoadedAction = movies => {
  return {
      type: types.MOVIES_LOADED,
      movies
  }
}

export const addMovie = movie => {
  return {
      type: types.ADD_MOVIE,
      movie
  }
}

export const removeMovie = movie => {
  return {
      type: types.REMOVE_MOVIE,
      movie
  }
}
