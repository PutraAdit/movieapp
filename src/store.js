import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './reducers/rootReducer'
import rootSaga from './sagas/rootSaga'

const initialState = {};

const saga = createSagaMiddleware()

const store = createStore(rootReducer, initialState, applyMiddleware(saga))

saga.run(rootSaga)

export default store
